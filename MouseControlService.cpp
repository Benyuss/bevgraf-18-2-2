#include "MouseControlService.h"
#include "DistanceCalculator.h"
#include "Configuration.h"

MouseControlService::MouseControlService() {}

MouseControlService::MouseControlService(GamePresenter gamePresenter) {
	this->gamePresenter = gamePresenter;
}

void MouseControlService::processMouse(int button, int action, int xMouse, int yMouse) {
	int i;
	if (button == GLUT_LEFT_BUTTON && action == GLUT_DOWN) {
		if (gamePresenter.isClickFoundAMovableObject(Point(xMouse, yMouse))) {
			gamePresenter.onMouseButtonDown(); //todo focused point save //todo observer pattern
			isDragged = true;
		}
		else {
			isDragged = false;
		}
	}
	if (button == GLUT_LEFT_BUTTON && action == GLUT_UP) {
		gamePresenter.onMouseButtonUp();
		isDragged = false;
	}
}

void MouseControlService::processMouseActiveMotion(int xMouse, int yMouse) {
	if (isDragged) {
		gamePresenter.onMouseMovement(Point(xMouse, yMouse));
	}
}