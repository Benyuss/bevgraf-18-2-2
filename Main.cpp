#define _USE_MATH_DEFINES
#include <GL/glut.h>
#include "Configuration.h"
#include "GamePresenter.h"
#include "MouseControlService.h"

GamePresenter gamePresenter;
MouseControlService mouseService;

void processMouse(GLint button, GLint action, GLint xMouse, GLint yMouse)
{
	mouseService.processMouse(button, action, xMouse, yMouse);
}

void processMouseActiveMotion(GLint xMouse, GLint yMouse)
{
	mouseService.processMouseActiveMotion(xMouse, yMouse);
}


void init(void)
{
	gamePresenter = GamePresenter();
	mouseService = MouseControlService(gamePresenter);

	glClearColor(1.0, 1.0, 1.0, 0.0);
	glMatrixMode(GL_PROJECTION);			// Set projection parameters.
	gluOrtho2D(0.0, WINDOW_WIDTH, 0.0, WINDOW_HEIGHT);

	gamePresenter.initialize();
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);

	gamePresenter.drawObjects();

	glutSwapBuffers();
}

void update(int n)
{
	glutPostRedisplay();

	glutTimerFunc(10, update, 0);
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	glutInitWindowPosition(INIT_POSITION_X, INIT_POSITION_Y);
	glutCreateWindow("Introduction to Computer Graphics - Home Project - 2 - Boros Bence - VGY4TX");
	init();

	glutDisplayFunc(display);
	glutMouseFunc(processMouse);
	glutMotionFunc(processMouseActiveMotion);
	glutTimerFunc(10, update, 0);
	glutMainLoop();
	return 0;
}