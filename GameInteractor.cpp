#include "GameInteractor.h"
#include "PointGenerator.h"

GameInteractor::GameInteractor()
{
	windowParameter = 0.5;

	gameState = GameState::HIDE_ALL;
}

void GameInteractor::initialize() {
	draggablePoints.clear();

	draggablePoints = generateDefaultPoints();
	windowParameter = 0.5;

	gameState = GameState::HIDE_ALL;
}

void GameInteractor::setPointsUndragged() {
	for (auto& carPoint : draggablePoints)
	{
		carPoint.setDragged(false);
	}
}

std::vector<DraggablePoint> &GameInteractor::getDraggablePoints() {
	return this->draggablePoints;
}

bool GameInteractor::pointLocationIsValid(Point point) {
	for (const auto draggablePoint : draggablePoints) {
		const auto distance = DistanceCalculator::distanceOfPoints(draggablePoint, point);
		if (distance < BUTTON_NEUTRAL_RADIUS) {

			return true;
		}
	}

	return false;
}

void GameInteractor::savePoint(Point point) {
	int minDistance = LONG_MAX;
	auto minIndex = 0;

	for (auto i = 0; i < draggablePoints.size(); i++) {
		const auto distance = DistanceCalculator::distanceOfPoints(draggablePoints.at(i), point);
		if (distance < BUTTON_NEUTRAL_RADIUS) {
			if (minDistance > distance) {
				minDistance = distance;
				minIndex = i;
			}
		}
	}
	draggablePoints.at(minIndex).setDragged(true);
}

GameState &GameInteractor::getGameState() {
	return this->gameState;
}

void GameInteractor::setGameState() {
	if (gameState == GameState::SHOW_POINTS) {
		this->gameState = GameState::SHOW_ALL;
	}
	else if (gameState == GameState::HIDE_ALL) {
		this->gameState = GameState::SHOW_POINTS;
	}
	else if (gameState == GameState::SHOW_ALL) {
		this->gameState = GameState::HIDE_ALL;
	}
}

float &GameInteractor::getWindowParameter() {
	return this->windowParameter;
}

void GameInteractor::setWindowParameter(float windowParameter) {
	if (windowParameter >= 0.0 && windowParameter <= 1.0) {
		this->windowParameter = windowParameter;
	}
}