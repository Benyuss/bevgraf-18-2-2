#include "Circle.h"

Circle::Circle() {
}

Circle::Circle(Point center, double radius) {
	this->center = center;
	this->radius = radius;
}

Circle::Circle(Point center, double radius, Colorable color) {
	this->center = center;
	this->radius = radius;
	this->setColors(color);
}

double Circle::getRadius() {
	return radius;
}

void Circle::setRadius(double radius) {
	this->radius = radius;
}

void Circle::setColors(Colorable color) {
	this->setColor(color.getRed(), color.getGreen(), color.getBlue());
}