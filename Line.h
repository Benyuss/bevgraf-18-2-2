#pragma once

#include "point.h"
#include "Colorable.h"

class Line : public Colorable
{
public:
	Line();
	Line(Point, Point, float);
	Line(Point, Point, float, double, double, double);

	float getWidth();
	void setWidth(float width);

	Point getAPoint();
	void setAPoint(Point a);

	Point getBPoint();
	void setBPoint(Point b);

private:
	float width;
	Point a, b;
};