#pragma once
class Colorable
{
public:
	Colorable();
	Colorable(double, double, double);

	double getRed();
	double getGreen();
	double getBlue();
	void setColor(double, double, double);
	void setColor(Colorable color);

private:
	double red, green, blue;
};