#include "line.h"

Line::Line() {}

Line::Line(Point a, Point b, float width)
{
	this->a = a;
	this->b = b;
	this->width = width;
}

Line::Line(Point a, Point b, float width, double red, double green, double blue)
{
	this->a = a;
	this->b = b;
	this->width = width;
	this->setColor(red, green, blue);
}

float Line::getWidth()
{
	return this->width;
}

void Line::setWidth(float width)
{
	this->width = width;
}

Point Line::getAPoint() {
	return this->a;
}

void Line::setAPoint(Point a) {
	this->a = a;
}

Point Line::getBPoint() {
	return this->b;
}

void Line::setBPoint(Point b) {
	this->b = b;
}