#pragma once
#include "line.h"
#include "Point.h"
#include "Circle.h"

class DistanceCalculator
{
public:
	
	static float distanceOfLineAndPoint(Line l, Point p);
	static float distanceOfPoints(Point a, Point b);
	static bool isCircleContainsPoint(Circle circle, Point point);
	static bool isCircleContainsCircle(Circle circle1, Circle circle2);
	static bool isPointOnLeftSideOfLine(Point point, Line line);
};