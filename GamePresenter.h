#pragma once
#include "Point.h"
#include "Circle.h"

class GamePresenter
{
public:
	GamePresenter();

	/*
		A j�t�k inicializ�ci�ja.
	*/
	void initialize();

	/*
		A j�t�k objektumainak rajzol�sa.
	*/
	void drawObjects();

	void drawDraggablePoints();
	  
	void onMouseMovement(Point mouseLocation);
	void onMouseButtonUp();
	void onMouseButtonDown();

	bool isClickFoundAMovableObject(Point mouseLocation); //todo for removl -> observer pattern

private:

	void initializeGameArea();
};