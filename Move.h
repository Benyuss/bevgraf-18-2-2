#pragma once
#include "point.h"

class Move
{
public:
	Move();
	Move(double,Point);
	void negation(char b);
	Point mirroring(Point a, Point b);

	Point v;
};