#include "move.h"
#include "Configuration.h"

Move::Move()
{}

Move::Move(double step, Point a)
{
	this->v.setX((a.getX() > WINDOW_WIDTH / 2) ? -step : step);
	this->v.setY((a.getY() > WINDOW_HEIGHT / 2) ? -step : step);
}

void Move::negation(char b)
{
	switch (b)
	{
	case 'x':
		this->v.setX(v.getX() * -1);
		break;

	case 'y':
		this->v.setY(v.getY() * -1);
	}
}

Point Move::mirroring(Point pointToMirror, Point mirror)
{
	float t = (pointToMirror.getX()*mirror.getX() + pointToMirror.getY()*mirror.getY()) / (mirror.getX()*mirror.getX() + mirror.getY()*mirror.getY());
	Point c = Point(mirror.getX()*t, mirror.getY()*t);
	Point ac = Point(c.getX() - pointToMirror.getX(), c.getY() - pointToMirror.getY());
	return Point(pointToMirror.getX() + 2 * ac.getX(), pointToMirror.getY() + 2 * ac.getY());
}