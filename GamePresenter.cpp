﻿#include "GamePresenter.h"
#include "Line.h"
#include "Configuration.h"
#include "Circle.h"
#include "ObjectDrawer.h"
#include "DraggablePoint.h"
#include "GameInteractor.h"
#include "bevgrafmath2018.h"
#include <cmath>

GameInteractor gameInteractor;

bool isGameRunning;

GamePresenter::GamePresenter() {
	isGameRunning = false;
	gameInteractor = GameInteractor();
}

void GamePresenter::initialize() {
	gameInteractor.initialize();
}

void GamePresenter::drawDraggablePoints() {
	for (int i = 0; i < gameInteractor.getDraggablePoints().size(); i++)
	{
		if (i != 4) {
			ObjectDrawer::drawPoint(gameInteractor.getDraggablePoints().at(i),
				Colorable(COLOR_DRAGGABLE_POINT_RED, COLOR_DRAGGABLE_POINT_GREEN, COLOR_DRAGGABLE_POINT_BLUE), DRAGGABLE_POINT_SIZE);
		}
		else
		{
			ObjectDrawer::drawPoint(Point(0, 0),
				Colorable(COLOR_DRAGGABLE_POINT_RED, COLOR_DRAGGABLE_POINT_GREEN, COLOR_DRAGGABLE_POINT_BLUE), 0);
		}
	}

	for (auto point : gameInteractor.getDraggablePoints()) {

	}
}

void drawFirst() {
	double t[3] = { -1.0, 0, 1.0 };

	std::vector<DraggablePoint> points = gameInteractor.getDraggablePoints();

	mat24 G(vec2(points.at(3).getX(), points.at(3).getY()),
		vec2(points.at(2).getX(), points.at(2).getY()),
		vec2(points.at(1).getX(), points.at(1).getY()),
		vec2(points.at(0).getX() - points.at(1).getX(), points.at(0).getY() - points.at(1).getY()));

	vec4 mvectors[4] = {
	{ (float)pow(t[0],3.0f), (float)pow(t[0],2.0f), (float)t[0], 1.0f },
	{ (float)pow(t[1],3.0f), (float)pow(t[1],2.0f), (float)t[1], 1.0f },
	{ (float)pow(t[2],3.0f), (float)pow(t[2],2.0f), (float)t[2], 1.0f },
	{ 3 * (float)pow(t[2],2.0f), 2 * (float)t[2], 1.0f, 0.0f } };

	mat4 mi = { mvectors[0],mvectors[1],mvectors[2],mvectors[3] };
	mat4 M = inverse(transpose(mi));

	mat24 C = G * M;

	for (double i = t[0]; i <= t[2]; i = i + 0.001) {

		vec4 T(pow(i, 3), pow(i, 2), i, 1);
		vec2 Q = C * T;
		ObjectDrawer::drawPoint(
			Point(Q.x, Q.y),
			Colorable(COLOR_LINE_FIRST_RED, COLOR_LINE_FIRST_GREEN, COLOR_LINE_FIRST_BLUE),
			LINE_WIDTH);
	}

	ObjectDrawer::drawLine(Line(points.at(0), points.at(1), LINE_WIDTH));
}

double binomialCoefficient(int n, int k)
{
	if (k > n - k)
		k = n - k;

	double c = 1.0;

	for (int i = 0; i < k; i++)
	{
		c = c * (n - i);
		c = c / (i + 1);
	}
	return c;
}

void drawBezierWithBernstein(std::vector<DraggablePoint> points, Colorable color) {

	//TODO bezier gorbe pontjainak kiszamolasa bernstein polinommal
	int n = points.size() - 1;

	for (float u = 0.0; u < 1.0; u += 0.001) {
		Point nextPoint = Point(0, 0);
		for (int i = 0; i <= n; i++) {
			float actualBernsteinPolinom = binomialCoefficient(n, i) * pow(u, i)*pow(1.0 - u, n - i);
			nextPoint.setX(nextPoint.getX() + points.at(i).getX() * actualBernsteinPolinom);
			nextPoint.setY(nextPoint.getY() + points.at(i).getY() * actualBernsteinPolinom);
		}
		ObjectDrawer::drawPoint(nextPoint, color, LINE_WIDTH);
	}
}

//c1 continuity q1 = p4-p3 /n +q0
DraggablePoint getContinuousPoint()
{
	// P4 - P3
	Point p4 = gameInteractor.getDraggablePoints().at(0);
	Point p3 = gameInteractor.getDraggablePoints().at(1);

	Point tempPoint = Point((p4.getX() - p3.getX()) / 4.0, (p4.getY() - p3.getY()) / 4.0);
	Point q0 = gameInteractor.getDraggablePoints().at(1);
	return
	{
		Point
		(
			tempPoint.getX() + q0.getX(),
			tempPoint.getY() + q0.getY()
		),
		0
	};
}

void drawSecond() {
	std::vector<DraggablePoint> points;

	std::vector<DraggablePoint> points2 = gameInteractor.getDraggablePoints();

	double t[4] = { -1.0, 0, 1.0, 2.0 };
	vec4 T_der = { (float)(3 * t[0] * t[0]), float(2 * t[0]), 1.0, 0.0 };

	mat24 G(vec2(points2.at(6).getX(), points2.at(6).getY()),
		vec2(points2.at(7).getX(), points2.at(7).getY()),
		vec2(points2.at(8).getX(), points2.at(8).getY()),
		vec2(points2.at(9).getX(), points2.at(9).getY()));

	vec4 mvectors[4] = {
		{ (float)pow(t[0],3.0f), (float)pow(t[0],2.0f), (float)t[0], 1.0f },
	{ (float)pow(t[1],3.0f), (float)pow(t[1],2.0f), (float)t[1], 1.0f },
	{ (float)pow(t[2],3.0f), (float)pow(t[2],2.0f), (float)t[2], 1.0f },
	{ (float)pow(t[3],3.0f), (float)pow(t[3],2.0f), (float)t[3], 1.0f }
	};

	mat4 mi = { mvectors[0],mvectors[1],mvectors[2],mvectors[3] };
	mat4 M = inverse(transpose(mi));

	vec4 derived = 2 * ((G * M * T_der) / 4.0);
	Point tmp3 = Point(-derived.x + gameInteractor.getDraggablePoints().at(6).getX(),
		-derived.y + gameInteractor.getDraggablePoints().at(6).getY());
	DraggablePoint tmp4;
	tmp4.setPoint(tmp3);


	points.push_back(gameInteractor.getDraggablePoints().at(1));
	points.push_back(getContinuousPoint());
	points.push_back(gameInteractor.getDraggablePoints().at(5));
	points.push_back(tmp4);
	points.push_back(gameInteractor.getDraggablePoints().at(6));

	for (int i = 0; i < points.size(); i++)
	{
		ObjectDrawer::drawPoint(points.at(i),
			Colorable(COLOR_LINE_BEZIER_CONTROL_RED,
				COLOR_LINE_BEZIER_CONTROL_GREEN,
				COLOR_LINE_BEZIER_CONTROL_BLUE), 5);

	}

	ObjectDrawer::drawLine(Line(points.at(0), points.at(1), LINE_WIDTH));
	ObjectDrawer::drawLine(Line(points.at(1), points.at(2), LINE_WIDTH));
	ObjectDrawer::drawLine(Line(points.at(2), points.at(3), LINE_WIDTH));
	ObjectDrawer::drawLine(Line(points.at(3), points.at(4), LINE_WIDTH));

	drawBezierWithBernstein(points,
		Colorable(COLOR_LINE_SECOND_RED, COLOR_LINE_SECOND_GREEN, COLOR_LINE_SECOND_BLUE));
}

void drawThird() {
	double t[4] = { -1.0, 0, 1.0, 2.0 };

	std::vector<DraggablePoint> points = gameInteractor.getDraggablePoints();

	mat24 G(vec2(points.at(9).getX(), points.at(9).getY()),
		vec2(points.at(8).getX(), points.at(8).getY()),
		vec2(points.at(7).getX(), points.at(7).getY()),
		vec2(points.at(6).getX(), points.at(6).getY()));

	vec4 mvectors[4] = {
	{ (float)pow(t[0],3.0f), (float)pow(t[0],2.0f), (float)t[0], 1.0f },
	{ (float)pow(t[1],3.0f), (float)pow(t[1],2.0f), (float)t[1], 1.0f },
	{ (float)pow(t[2],3.0f), (float)pow(t[2],2.0f), (float)t[2], 1.0f },
	{ (float)pow(t[3],3.0f), (float)pow(t[3],2.0f), (float)t[3], 1.0f }
	};

	mat4 mi = { mvectors[0],mvectors[1],mvectors[2],mvectors[3] };
	mat4 M = transpose(inverse(mi));

	mat24 C = G * M;

	for (double i = t[0]; i < t[3]; i = i + 0.001) {

		vec4 T(pow(i, 3), pow(i, 2), i, 1);
		vec2 Q = C * T;
		ObjectDrawer::drawPoint(Point(Q.x, Q.y),
			Colorable(COLOR_LINE_THIRD_RED, COLOR_LINE_THIRD_GREEN, COLOR_LINE_THIRD_BLUE), LINE_WIDTH);
	}
}

void GamePresenter::drawObjects() {
	drawDraggablePoints();

	drawFirst();
	drawSecond();
	drawThird();
}

bool isMouseLocationInsideOfTheWindow(Point location) {
	if (location.getX() >= WINDOW_WIDTH || location.getX() <= 0) {
		return false;
	}
	else if (location.getY() >= WINDOW_WIDTH || location.getY() <= 0) {
		return false;
	}
	else {
		return true;
	}
}

void GamePresenter::onMouseMovement(Point mouseLocation) {
	mouseLocation.setY(WINDOW_HEIGHT - mouseLocation.getY());
	if (isMouseLocationInsideOfTheWindow(mouseLocation)) {
		for (int i = 0; i < gameInteractor.getDraggablePoints().size(); i++) {
			if (gameInteractor.getDraggablePoints().at(i).isDragged()) {
				gameInteractor.getDraggablePoints().at(i).setPoint(mouseLocation);
			}
		}
	}
}

void GamePresenter::onMouseButtonUp() {
	//kor szinezes
	gameInteractor.setPointsUndragged();

	drawObjects();
}

void GamePresenter::onMouseButtonDown() {
	//kor szinezes - focused pont?
}

bool GamePresenter::isClickFoundAMovableObject(Point mouseLocation) {
	mouseLocation.setY(WINDOW_HEIGHT - mouseLocation.getY());

	if (gameInteractor.pointLocationIsValid(mouseLocation)) {
		gameInteractor.savePoint(mouseLocation);
		return true;
	}
	else {
		return false;
	}
}