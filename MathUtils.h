#pragma once
#include "Line.h"
#include "Point.h"

class MathUtils
{
public:
	MathUtils();

	double static detOfMatrix2x2(double matrix[2][2]);

	Point static getIntersection(Point point1, Point point2, Line line);

};