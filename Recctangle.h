#pragma once
#include "Point.h"
#include "Colorable.h"

class Recctangle : public Colorable
{
public:
	Recctangle();
	Recctangle(Point bottomLeft, Point topRight, Colorable color);
	Recctangle(Point bottomLeft, Point bottomRight, Point topLeft, Point topRight, Colorable color);

	Point& getPointBottomLeft();
	void setPointBottomLeft(Point bottomLeft);

	Point& getPointBottomRight();
	void setPointBottomRight(Point bottomRight);

	Point& getPointTopRight();
	void setPointTopRight(Point topRight);

	Point& getPointTopLeft();
	void setPointTopLeft(Point topLeft);

	void calculateMissingPoints();

private:

	Point bottomRight, topLeft;
	Point bottomLeft, topRight;
};