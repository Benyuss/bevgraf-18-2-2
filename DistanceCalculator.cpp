#include "DistanceCalculator.h"
#include <cmath>

float DistanceCalculator::distanceOfLineAndPoint(Line line, Point point) {
	float abyDiff = line.getAPoint().getY() - line.getBPoint().getY();
	float baxDiff = line.getBPoint().getX() - line.getAPoint().getX();

	return fabs(((abyDiff)*line.getAPoint().getX() + (baxDiff)*line.getAPoint().getY() - (abyDiff)*point.getX() - (baxDiff)*point.getY()) / sqrt((abyDiff)*(abyDiff)+(baxDiff)*(baxDiff)));
}

float DistanceCalculator::distanceOfPoints(Point a, Point b) {

	return sqrt(pow(a.getX() - b.getX(), 2) + pow(a.getY() - b.getY(), 2));
}

bool DistanceCalculator::isCircleContainsCircle(Circle circle1, Circle circle2) {
	float distance = distanceOfPoints(circle1.center, circle2.center);

	return distance <= (circle1.getRadius() + circle2.getRadius());
}

bool DistanceCalculator::isCircleContainsPoint(Circle circle, Point point) {
	float distance = distanceOfPoints(circle.center, point);

	return distance <= circle.getRadius();
}

bool DistanceCalculator::isPointOnLeftSideOfLine(Point point, Line line) {
	return (line.getBPoint().getX() - line.getAPoint().getX()) * (point.getY() - line.getAPoint().getY())
		- (line.getBPoint().getY() - line.getAPoint().getY()) * (point.getX() - line.getAPoint().getX()) > 0;
}