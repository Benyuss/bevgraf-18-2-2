#pragma once
#include "Circle.h"
#include "Line.h"
#include "Recctangle.h"

class ObjectDrawer
{
public:

	/*
	Lerajzolja a megadott k�rt.
	Az empty boolean megadja, hogy a k�r teli vagy �res legyen-e.
	*/
	static void drawCircle(Circle circle, bool isEmpty);

	static void drawPolygon(Circle circle, int edges, bool isEmpty);

	static void drawLine(Line line);
	static void drawLine(std::vector<Point> lineElement, Colorable color, double pointSize);

	static void drawPoint(Point point, Colorable color, double pointSize);

	static void drawBackground(Colorable color);

	static void drawRectangle(Recctangle rectangle);
};