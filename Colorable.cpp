#include "Colorable.h"

Colorable::Colorable()
{
}

Colorable::Colorable(double red, double green, double blue) {
	this->red = red;
	this->green = green;
	this->blue = blue;
}

double Colorable::getRed() {
	return this->red;
}

double Colorable::getGreen() {
	return this->green;
}

double Colorable::getBlue() {
	return this->blue;
}

void Colorable::setColor(double red, double green, double blue) {
	this->red = red;
	this->green = green;
	this->blue = blue;
}

void Colorable::setColor(Colorable color) {
	this->red = color.getRed();
	this->green = color.getGreen();
	this->blue = color.getBlue();
}