#pragma once
#include <vector>
#include "DraggablePoint.h"
#include "Configuration.h"

std::vector<DraggablePoint> generateDefaultPoints() {
	std::vector<DraggablePoint> draggablePoints;

	draggablePoints.push_back(DraggablePoint(WINDOW_WIDTH / 10, 10, BUTTON_SENSITIVITY));
	draggablePoints.push_back(DraggablePoint(WINDOW_WIDTH / 10 + 40, 50, BUTTON_SENSITIVITY));
	draggablePoints.push_back(DraggablePoint(WINDOW_WIDTH / 10 + 80, 50, BUTTON_SENSITIVITY));
	draggablePoints.push_back(DraggablePoint(WINDOW_WIDTH / 10 + 170, 130, BUTTON_SENSITIVITY));

	draggablePoints.push_back(DraggablePoint(WINDOW_WIDTH / 10 + 60, 80, BUTTON_SENSITIVITY));
	draggablePoints.push_back(DraggablePoint(WINDOW_WIDTH / 10 + 70, 130, BUTTON_SENSITIVITY));
	draggablePoints.push_back(DraggablePoint(WINDOW_WIDTH / 10 + 80, 180, BUTTON_SENSITIVITY));
	draggablePoints.push_back(DraggablePoint(WINDOW_WIDTH / 10 + 200, 320, BUTTON_SENSITIVITY));

	draggablePoints.push_back(DraggablePoint(WINDOW_WIDTH / 10 + 400, 280, BUTTON_SENSITIVITY));
	draggablePoints.push_back(DraggablePoint(WINDOW_WIDTH / 10 + 530, 180, BUTTON_SENSITIVITY));
	return draggablePoints;
}