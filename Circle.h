#pragma once
#include "point.h"
#include "Utils.h"
#include "Colorable.h"

class Circle : public Colorable
{
public:
	Circle();
	Circle(Point, double);
	Circle(Point, double, Colorable);

	double getRadius();
	void setRadius(double);

	void setColors(Colorable);

	Point center;

private:
	double radius;
};