#include "MathUtils.h"

MathUtils::MathUtils() {}

double MathUtils::detOfMatrix2x2(double matrix[2][2]) {
	return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
}

/*
https://hu.wikipedia.org/wiki/Determin%C3%A1ns#2%C3%972-es
Determin�ns sz�m�t�s

https://stackoverflow.com/questions/20677795/how-do-i-compute-the-intersection-point-of-two-lines-in-python/20679579#20679579
metsz�spont sz�mol�s
*/
Point  MathUtils::getIntersection(Point point1, Point point2, Line line) {
	double a1 = line.getAPoint().getY() - line.getBPoint().getY();
	double b1 = line.getBPoint().getX() - line.getAPoint().getX();
	double c1 = (line.getBPoint().getY() - line.getAPoint().getY()) * line.getAPoint().getX() - (line.getBPoint().getX() - line.getAPoint().getX()) * line.getAPoint().getY();

	double a2 = point1.getY() - point2.getY();
	double b2 = point2.getX() - point1.getX();
	double c2 = (point2.getY() - point1.getY()) * point1.getX() - (point2.getX() - point1.getX()) * point1.getY();

	double A[2][2];
	A[0][0] = a1;
	A[0][1] = b1;
	A[1][0] = a2;
	A[1][1] = b2;

	double Dx[2][2];
	Dx[0][0] = -c1;
	Dx[0][1] = b1;
	Dx[1][0] = -c2;
	Dx[1][1] = b2;

	double Dy[2][2];
	Dy[0][0] = a1;
	Dy[0][1] = -c1;
	Dy[1][0] = a2;
	Dy[1][1] = -c2;

	double detA = detOfMatrix2x2(A);
	double detX = detOfMatrix2x2(Dx);
	double detY = detOfMatrix2x2(Dy);

	if (detA != 0) {
		double x = detX / detA;
		double y = detY / detA;

		return Point(x, y);
	}
}
