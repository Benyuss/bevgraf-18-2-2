#include "ObjectDrawer.h"
#include "bevgrafmath2018.h"
#include "Configuration.h"
#include <GL/glut.h>
#include "Recctangle.h"

void ObjectDrawer::drawCircle(Circle circle, bool isEmpty) {

	glColor3d(circle.getRed(), circle.getGreen(), circle.getBlue());
	if (isEmpty) {
		glBegin(GL_LINE_STRIP);
	}
	else {
		glBegin(GL_TRIANGLE_FAN);
	}

	for (double i = 0; i <= 2 * pi(); i += CIRCLE_DRAW_SET)
	{
		glVertex2d(circle.center.getX() + circle.getRadius()*cos(i), circle.center.getY() + circle.getRadius()*sin(i));
	}

	glEnd();
}

void ObjectDrawer::drawPolygon(Circle circle, int edges, bool isEmpty) {
	glColor3d(circle.getRed(), circle.getGreen(), circle.getBlue());
	if (isEmpty) {
		glBegin(GL_LINE_STRIP);
	}
	else {
		glBegin(GL_TRIANGLE_FAN);
	}

	for (double i = 0; i <= 2 * pi(); i += 2 * pi() / edges)
	{
		glVertex2d(circle.center.getX() + circle.getRadius()*cos(i), circle.center.getY() + circle.getRadius()*sin(i));
	}

	glEnd();
}

void ObjectDrawer::drawLine(Line line) {

	glColor3f(line.getRed(), line.getGreen(), line.getBlue());

	glBegin(GL_LINES);
	glVertex2f(line.getAPoint().getX(), line.getAPoint().getY());
	glVertex2f(line.getBPoint().getX(), line.getBPoint().getY());

	glEnd();
}

void ObjectDrawer::drawLine(std::vector<Point> lineElement, Colorable color, double pointSize) {
	glColor3f(color.getRed(), color.getGreen(), color.getBlue());

	glBegin(GL_LINE_STRIP);

	for (auto point : lineElement) {
		glVertex2d(point.getX(), point.getY());
	}
	
	glEnd();
}

void ObjectDrawer::drawPoint(Point point, Colorable color, double pointSize) {
	glColor3f(color.getRed(), color.getGreen(), color.getBlue());

	glPointSize(pointSize);
	glEnable(GL_POINT_SMOOTH);
	glBegin(GL_POINTS);

	glVertex2d(point.getX(), point.getY());

	glEnd();
}

void ObjectDrawer::drawBackground(Colorable color) {
	glClearColor(color.getRed(), color.getGreen(), color.getBlue(), 0.0);		// Set display-window color to white
}

void ObjectDrawer::drawRectangle(Recctangle rectangle) {
	glColor3d(rectangle.getRed(), rectangle.getGreen(), rectangle.getBlue());

	glBegin(GL_POLYGON);

	glVertex2d(rectangle.getPointTopLeft().getX(), rectangle.getPointTopLeft().getY());
	glVertex2d(rectangle.getPointBottomLeft().getX(), rectangle.getPointBottomLeft().getY());
	glVertex2d(rectangle.getPointBottomRight().getX(), rectangle.getPointBottomRight().getY());
	glVertex2d(rectangle.getPointTopRight().getX(), rectangle.getPointTopRight().getY());

	glEnd();
}