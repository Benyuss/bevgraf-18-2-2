#pragma once
#include "GamePresenter.h"
#include <GL/glut.h>

class MouseControlService
{
public:
	MouseControlService();
	MouseControlService(GamePresenter gamePresenter);

	void processMouse(int button, int action, int xMouse, int yMouse);
	void processMouseActiveMotion(int xMouse, int yMouse);

private:
	bool isDragged = false;

	GamePresenter gamePresenter;
};