#pragma once

class Point
{
public:
	Point();
	Point(double, double);
	
	double getX();
	void setX(double x);

	double getY();
	void setY(double y);

protected:
	double x, y;
};