#pragma once

const double WINDOW_HEIGHT = 600.0;
const double WINDOW_WIDTH = 800.0;
const double INIT_POSITION_X = 100;
const double INIT_POSITION_Y = 100;

const double CIRCLE_DRAW_SET = 0.01;
const double BUTTON_SENSITIVITY = 8;
const double BUTTON_NEUTRAL_RADIUS = BUTTON_SENSITIVITY * 2;

const double DRAGGABLE_POINT_SIZE = BUTTON_SENSITIVITY;
const double COLOR_DRAGGABLE_POINT_RED = 0.0;
const double COLOR_DRAGGABLE_POINT_GREEN = 0.278;
const double COLOR_DRAGGABLE_POINT_BLUE = 0.671;

const double LINE_WIDTH = 2;

const double COLOR_LINE_FIRST_RED = 1.0;
const double COLOR_LINE_FIRST_GREEN = 0.0;
const double COLOR_LINE_FIRST_BLUE = 1.0;

const double COLOR_LINE_SECOND_RED = 0.0;
const double COLOR_LINE_SECOND_GREEN = 1.0;
const double COLOR_LINE_SECOND_BLUE = 1.0;

const double COLOR_LINE_THIRD_RED = 1.0;
const double COLOR_LINE_THIRD_GREEN = 1.0;
const double COLOR_LINE_THIRD_BLUE = 0.0;

const double COLOR_LINE_BEZIER_CONTROL_RED = 1.0;
const double COLOR_LINE_BEZIER_CONTROL_GREEN = 0.929;
const double COLOR_LINE_BEZIER_CONTROL_BLUE = 0.0;
