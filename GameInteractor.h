#pragma once
#include <utility>
#include "Point.h"
#include "MathUtils.h"
#include "Configuration.h"
#include "Circle.h"
#include "Move.h"
#include "DistanceCalculator.h"
#include "DraggablePoint.h"
#include "GameState.h"

class GameInteractor
{
public:
	GameInteractor();

	void initialize();

	void setPointsUndragged();
	bool pointLocationIsValid(Point point);

	std::vector<DraggablePoint> &getDraggablePoints();
	void savePoint(Point point);

	GameState &getGameState();
	void setGameState();

	void setWindowParameter(float windowParameter);
	float &getWindowParameter();
private:

	GameState gameState;
	std::vector<DraggablePoint> draggablePoints;

	float windowParameter;
};