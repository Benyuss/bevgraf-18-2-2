#pragma once
#include "Point.h"
class DraggablePoint : public Point
{
public:
	DraggablePoint();
	DraggablePoint(double x, double y, int sensitivityRadius);
	DraggablePoint(Point point, int sensitivityRadius);

	bool isDragged();
	void setDragged(bool mDragged);

	int getSensitivity();
	void setSensitivity(int sensitivityRadius);

	Point getPoint();
	void setPoint(Point point);

private:
	bool dragged;
	int sensitivityRadius;
};