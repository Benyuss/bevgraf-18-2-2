#include "Recctangle.h"

Recctangle::Recctangle() {}

Recctangle::Recctangle(Point bottomLeft, Point topRight, Colorable color) {
	this->bottomLeft = bottomLeft;
	this->topRight = topRight;
	this->setColor(color);

	calculateMissingPoints();
}

Recctangle::Recctangle(Point bottomLeft, Point bottomRight, Point topLeft, Point topRight, Colorable color) {
	this->bottomLeft = bottomLeft;
	this->bottomRight = bottomRight;
	this->topLeft = topLeft;
	this->topRight = topRight;
	this->setColor(color);
}

Point& Recctangle::getPointBottomLeft() {
	return this->bottomLeft;
}

void Recctangle::setPointBottomLeft(Point bottomLeft) {
	this->bottomLeft = bottomLeft;
}

Point& Recctangle::getPointBottomRight() {
	return this->bottomRight;
}

void Recctangle::setPointBottomRight(Point bottomRight) {
	this->bottomRight = bottomRight;
}

Point& Recctangle::getPointTopRight() {
	return this->topRight;
}

void Recctangle::setPointTopRight(Point topRight) {
	this->topRight = topRight;
}

Point& Recctangle::getPointTopLeft() {
	return this->topLeft;
}

void Recctangle::setPointTopLeft(Point topLeft) {
	this->topLeft = topLeft;
}

void Recctangle::calculateMissingPoints() {
	this->bottomRight = Point(this->topRight.getX(), this->bottomLeft.getY());
	this->topLeft = Point(bottomLeft.getX(), this->topRight.getY());
}