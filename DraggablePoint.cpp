#include "DraggablePoint.h"

DraggablePoint::DraggablePoint() {

	setDragged(false);
}

DraggablePoint::DraggablePoint(double x, double y, int sensitivityRadius) {
	this->x = x;
	this->y = y;
	this->sensitivityRadius = sensitivityRadius;

	setDragged(false);
}

DraggablePoint::DraggablePoint(Point point, int sensitivityRadius) {
	this->x = point.getX();
	this->y = point.getY();
	this->sensitivityRadius = sensitivityRadius;

	setDragged(false);
}

bool DraggablePoint::isDragged() {
	return this->dragged;
}

void DraggablePoint::setDragged(bool dragged) {
	this->dragged = dragged;
}

int DraggablePoint::getSensitivity() {
	return this->sensitivityRadius;
}

void DraggablePoint::setSensitivity(int sensitivityRadius) {
	if (sensitivityRadius == 0) {
		sensitivityRadius = 1;
	}
	this->sensitivityRadius = sensitivityRadius;
}

Point DraggablePoint::getPoint() {
	return Point(this->x, this->y);
}

void DraggablePoint::setPoint(Point point) {
	this->setX(point.getX());
	this->setY(point.getY());
}